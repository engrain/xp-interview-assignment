Submission and Clean up
=======================

You've completed the assignment—well done! Now, follow these final steps to package your work and clean up your environment.

## Packaging Your Work

To ensure all necessary files, including the `.git` directory, are included, **please refer to the packaging instructions in the `README.md` file at the root of the project**. The provided scripts will handle the process automatically.

## Clean Up

To remove installed dependencies, run:

```sh
$ rm -rf client/node_modules && rm -rf server/vendor`
```

To stop and remove all Docker containers and volumes used during the assignment, run:

```sh
$ docker compose down -v
```

To remove all Docker images downloaded or built during the assignment (optional):

```
$ docker rmi $(docker images -q)
```

*Note: If you have other Docker projects, you may want to skip this step.*

After packaging your assignment using the provided instructions in `README.md` at the root of the project, submit the `interview_assignment.zip` file as per the instructions.

Thank you for completing the assessment. 🚀
