import { combineReducers } from 'redux-immutable'
import calculator from 'app/reducers/calculator'

export default combineReducers({
  calculator,
})
