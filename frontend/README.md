Interview Assignment
====================

Welcome to the Senior frontend developer assessment. This assessment is designed to evaluate your skills and knowledge as a senior frontend developer, specifically in the areas of React and Redux. You will be responsible for understanding and demonstrating a deep understanding of both technologies at a senior level. The assessment will consist of a series of tasks and questions that will test your ability to solve problems, write clean and maintainable code, and make informed decisions.

Here are some links to the documentation that may be helpful during the assessment:

[Unit Map Developer Hub](https://developers.unitmap.com/)

**Note** The Laravel application is already set up to display a list of Units for you, you will need to follow installation instructions to get the backend API working.

We recommend that you take the time to review the documentation and any relevant materials before you begin.

You will be evaluated on the following:

* Code readability and organization
* Proper usage of the Unit Map API
* Handling of errors and pagination
* Code documentation and commenting

## Installation

You'll need Docker on your machine in order to configure and run the required services. Install [Docker Desktop for Mac](https://docs.docker.com/desktop/install/mac-install/) if you have not done so already. Run `docker -v` to verify you have version `20.10.17` or greater installed.

Clone https://bitbucket.org/engrain/xp-interview-assignment to your machine. Once cloned, within the working directory run `docker compose up -d`.

> The process may take several minutes as Docker downloads, builds, and runs the services declared in `docker-compose.yml`.

Next run `docker compose exec app composer install` to install dependant composer packages.

Once finished, open `http://localhost` in your Browser and you should see the Engrain logo. This indicates the installation was successful.

Create a new branch named `my-work`. This branch will be used to commit your progress.

```
$ git checkout -b my-work
```

---

The application uses Webpack to compile our source files. Run `docker compose run --rm node` to start a shell session inside the `node` container. This container includes the required Node.js and Yarn versions. The remaining commands are expected to be run from this shell.

> Run `exit` to terminate the shell session and return to your host terminal.

Install the NPM dependencies and begin watching the client code for changes.

```
# yarn install
# yarn watch
```

Once Webpack finishes the initial compilation, open `http://localhost/` in your Browser and continue with the instructions displayed onscreen.

Assessment
==========

Use the provided React application to load units from an API endpoint and display them on the map

Use the provided React application, which already has the Unit Map library set up. Visit localhost to see the interactive map. Please read the Unit Map API documentation provided earlier in the instructions.

1. Your first task is to load the units into the provided Redux store from the endpoint http://localhost/api/v1/units. The Unit Ids are provided, and are important to completing the rest of the assignment.

2. The units should be displayed on the side of the application. You will not be judged based on the look and feel of the application.

3. After you've loaded all the unit data up into the Redux store, make each unit highlight when you hover over them.

4. After you've completed the highlighting task, add a click event to the units which will open up a modal to display their details. You should display all the information obtained from the API endpoint.

5. Once you've completed the above tasks, highlight each available unit using the color pink.

Note: The design of the application is not a focus, the main focus is on how you handle the data and how you implement the functionality.

Submission and Clean up
=======================

You've completed the assignment—well done! Now, follow these final steps to package your work and clean up your environment.

## Packaging Your Work

To ensure all necessary files, including the `.git` directory, are included, **please refer to the packaging instructions in the `README.md` file at the root of the project**. The provided scripts will handle the process automatically.

## Clean Up

To remove installed dependencies, run:

```sh
$ rm -rf client/node_modules && rm -rf server/vendor`
```

To stop and remove all Docker containers and volumes used during the assignment, run:

```sh
$ docker compose down -v
```

To remove all Docker images downloaded or built during the assignment (optional):

```
$ docker rmi $(docker images -q)
```

*Note: If you have other Docker projects, you may want to skip this step.*

After packaging your assignment using the provided instructions in `README.md` at the root of the project, submit the `interview_assignment.zip` file as per the instructions.

Thank you for completing the assessment. 🚀
