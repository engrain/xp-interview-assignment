<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

class Units extends Controller {

    public function index() {

        $units = [
            [
                'id' => '12307',
                'availability_date' => Carbon::now()->addDays(5)->format('Y-m-d'),
                'floor_plan' => '1 Bedroom',
                'short_description' => 'Spacious 1BR with city views.',
                'square_footage' => '700 sqft',
                'price' => '$2000/month'
            ],
            [
                'id' => '12172',
                'availability_date' => Carbon::now()->subDays(5)->format('Y-m-d'),
                'floor_plan' => '2 Bedroom',
                'short_description' => 'Luxurious 2BR with balcony.',
                'square_footage' => '1000 sqft',
                'price' => '$3000/month'
            ],
            [
                'id' => '12306',
                'availability_date' => Carbon::now()->addDays(2)->format('Y-m-d'),
                'floor_plan' => 'Studio',
                'short_description' => 'Cozy studio with modern amenities.',
                'square_footage' => '500 sqft',
                'price' => '$1500/month'
            ],
            [
                'id' => '12098',
                'availability_date' => Carbon::now()->addDays(30)->format('Y-m-d'),
                'floor_plan' => '3 Bedroom',
                'short_description' => 'Spacious 3BR with backyard.',
                'square_footage' => '1200 sqft',
                'price' => '$4000/month'
            ],
        ];

        return response()->json($units);
    }
}