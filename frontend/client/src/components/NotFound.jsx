import React from 'react'

export default ({ location }) => (
  <p style={{ margin: 20 }}>
    <i>{location.pathname}</i> not found.
  </p>
)
