import React, { useEffect, useRef, useState } from 'react'
import styled from 'react-emotion'

const UnitMapWrapper = styled('div')`
  width: 60%;
  height: 600px;
  border: 1px solid #0000004c;
`

const Title = styled('h1')`
  font-size: 14px;
  margin-bottom: 5px;
`

const Container = styled('div')`
  width: 100%;
  height: 100vh;
  justify-content: center;
  align-items: center;
  display: flex;
  flex-direction: column;
`

export default () => {
  const mapRef = useRef()
  const [ map, setMap ] = useState()

  useEffect(() => {
    if (!mapRef) {
      return
    }

    const map = unitmap(mapRef.current, 'fb710633807b4c39af027fbcd0ffeb65')
    map.load(698);
    setMap(map)
  
  }, [])

  useEffect(() => {
    if (!map) {
      return
    }

    // TODO: Highlight Active Units. Remember the UnitMap implementation is a purely
    // imperative Api, so you will have to query the `map` everytime you want to make
    // a change.

  }, [map])

  return <Container>
    <Title>Engrain, Inc. Front End React Assessment</Title>
    <UnitMapWrapper>
      <div ref={mapRef} id="map" style={{
        width: '100%',
        height: '100%',
      }} />
    </UnitMapWrapper>
  </Container>
}