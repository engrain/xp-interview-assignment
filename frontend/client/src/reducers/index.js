import { combineReducers } from 'redux-immutable'
import units from 'app/reducers/units'

export default combineReducers({
  units,
})
