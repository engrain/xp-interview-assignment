import { fromJS } from 'immutable'
import { handleActions } from 'redux-actions'
import * as actions from 'app/actions/calculator'

const initialState = fromJS({
  entities: []
})

export default handleActions(
  {
    [actions.ADD_UNITS]: (state, action) =>
      state,
  },
  initialState
)
