import React from 'react'
import ReactDOM  from 'react-dom/client'
import Root from 'app/containers/Root'

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(<Root />)
