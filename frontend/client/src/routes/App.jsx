import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Embed from 'app/components/Embed'
import NotFound from 'app/components/NotFound'

export default () => (
  <Switch>
    <Route exact path="/" component={Embed} />
    <Route component={NotFound} />
  </Switch>
)
