# PowerShell script to package the interview assignment, including .git
$zipFileName = "interview_assignment.zip"
$sourcePath = Get-Location
$destinationPath = "$sourcePath\$zipFileName"

# Remove old zip file if exists
if (Test-Path $destinationPath) {
    Remove-Item $destinationPath -Force
}

# Create zip including .git directory
Compress-Archive -Path "$sourcePath\*" -DestinationPath $destinationPath -Force

Write-Output "Assignment packaged successfully: $destinationPath"
