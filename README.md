This directory contains three folders, each of which pertains to a specific position in the interview process.

## Frontend
The frontend folder is intended for the Senior Frontend React Developer position. It contains all the necessary files and information for the frontend portion of the interview assignment. The goal of the frontend assignment is to assess the candidate's knowledge of React and their ability to create a functional and visually appealing user interface.

## Backend
The backend folder is intended for the Senior Backend Developer position. It contains all the necessary files and information for the backend portion of the interview assignment. The goal of the backend assignment is to assess the candidate's knowledge of server-side programming and their ability to use Laravel.

## Full-Stack
The full-stack folder is for the Full-Stack Developer position. It contains all the necessary files and information for the full-stack portion of the interview assignment. The goal of the full-stack assignment is to assess the candidate's ability to create a complete and functional application that integrates both the frontend and backend.

In each of these folders, you will find a detailed README file that provides more information on the specific assignment, including the requirements and any relevant setup instructions.

# Notes

Welcome to the Senior Backend Developer assessment!

This assignment is designed to evaluate your ability to **figure things out**—more than just writing perfect code. There may be challenges in getting everything up and running, and that’s expected. **We’re not looking for perfection; we’re looking to see how you approach solving problems.**

Your primary tool for getting started is **Docker**—everything you need is containerized, so lean on it to avoid local environment issues. If something isn’t working, use the provided resources, logs, and documentation to troubleshoot.

Most importantly, if you hit a roadblock, **show us how you work through it**—that’s what we care about the most.

Good luck, and have fun! 🚀

# Packaging Your Assignment for Submission

At the root of the project directory, you will find scripts to package your work before submission. These scripts ensure that **all necessary files, including the `.git` directory, are included** in the final zip file.

## For Windows Users (PowerShell)
1. Open **PowerShell** in the project directory.
2. Run the following command: `.\package_assignment.ps1`
3. This will generate a zip file named `interview_assignment.zip` in the current directory.

## For macOS/Linux Users (Bash)
1. Open **Terminal** in the project directory.
2. Run the following command: `./package_assignment.sh`
3. This will create `interview_assignment.zip` containing all necessary files.

## Final Steps
After running the script:
- Verify that `interview_assignment.zip` exists in your project directory.
- **Do not modify the contents manually**—the script ensures all required files are included.
- Submit `interview_assignment.zip` as per the assignment instructions.

This process ensures we can evaluate your Git history and project structure. 🚀

Once you have successfully packaged your assignment, please send the `interview_assignment.zip` file back to the point of contact you’ve been communicating with.
