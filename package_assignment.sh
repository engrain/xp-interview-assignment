#!/bin/bash

# Define the zip file name
ZIP_FILE="interview_assignment.zip"

# Remove old zip file if it exists
if [ -f "$ZIP_FILE" ]; then
    rm "$ZIP_FILE"
fi

# Zip the entire directory including .git
zip -r "$ZIP_FILE" . -x "package_assignment.sh" "package_assignment.ps1"

echo "Assignment packaged successfully: $ZIP_FILE"
