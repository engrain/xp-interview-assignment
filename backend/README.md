Interview Assignment
====================

Hello! To begin work on the assignment, please follow the instructions contained here.

Welcome to the Senior Backend Developer assessment. This assessment is designed to evaluate your skills and knowledge as a senior backend developer, specifically in the areas of PHP and Laravel. You will be responsible for understanding and demonstrating a deep understanding of both technologies at a senior level. The assessment will consist of a series of tasks and questions that will test your ability to solve problems, write clean and maintainable code, and make informed decisions.

Here are some links to the documentation that may be helpful during the assessment:

[SightMap API documentation](https://api.sightmap.com/v1/openapi)

**Note** The Laravel application is already set up, so you do not need to create a new one.

We recommend that you take the time to review the documentation and any relevant materials before you begin.

You will be evaluated on the following:

* Code readability and organization
* Proper usage of Laravel concepts and best practices
* Handling of errors and pagination
* Code documentation and commenting

## Installation

You'll need Docker on your machine in order to configure and run the required services. Install [Docker for Mac](https://docs.docker.com/docker-for-mac/install/) if you have not done so already. Run `docker -v` to verify you have version `20.10.17` or greater installed. We recommend staying up-to-date on the stable channel.

Clone https://bitbucket.org/engrain/xp-interview-assignment to your machine. Once cloned, within the working directory run `docker compose up -d`.

> The process may take several minutes as Docker downloads, builds, and runs the services declared in `docker-compose.yml`.

Next run `docker compose exec app composer install` to install dependant composer packages.

Once finished, open `http://localhost` in your Browser and you should see the Engrain logo. This indicates the installation was successful.

Create a new branch named `my-work`. This branch will be used to commit your progress.

```
$ git checkout -b my-work
```

Assessment: Part I
======================

***Task: Implement the SightMap REST API as a service class and use it in a Laravel application***

Using the documentation provided for the SightMap API, implement a service class that can handle the API calls. The library should be able to handle:

* Authentication
* Making GET, POST, PUT and DELETE requests for ONE resource you choose.
* Handling pagination

How you implement the service class is completly up to you. You can chose any resource you'd like to use. Once the service class is implemented, use it in a the provided laravel application to retrieve a list of all the items from the SightMap REST Api, store them in the database. Remember to create a migration file to match the chosen resource and migrate it.

To complete this task, the candidate needs to:

1. Implement a service class that can handle the API calls for a chosen resource, including authentication, GET, POST, PUT, DELETE requests, and pagination.
2. Write tests for the service class to ensure that it functions correctly and handles errors gracefully.
3. Use Mockery to mock the API calls in the tests to avoid making actual requests to the SightMap API.
4. Create a migration file that matches the chosen resource and migrate it in the Laravel application.
5. Use the service class to retrieve a list of all the items from the SightMap REST API and store them in the database.

The use of Mockery is important to ensure that the tests are not affected by external factors, such as the availability of the SightMap API or changes to its response format. By mocking the API calls, the tests can be run reliably and repeatedly without causing any harm or unexpected side-effects to the actual API.

Please remember to review the SightMap REST API documentation for example responses related to the chosen resource. This will help you design and test the service class more effectively and catch any potential issues early on.

Assessment: Part II
====================

Please answer the following questions here in the `README.md` file. Please commit them with your project.

1. How would you handle database migrations when upgrading to a new version of Laravel?
2. Can you explain the role of controllers in a Laravel application?
3. Can you explain how you would optimize a slow performing query in a Laravel application?
4. Describe a feature you would add to your Laravel application to increase security.
5. The following code returns an error, please explain why and how you would fix it?

```php
class ProductController extends Controller {
    public function index() {
        $products = Product::all();
        return view('products.index', ['products' => $products]);
    }
}
```

6. How would you fix the following error in the following code?

```php
class OrderController extends Controller {
    public function store(Request $request) {
        $order = new Order();
        $order->user_id = Auth::user()->id;
        $order->total = $request->total;
        $order->save();
        return redirect()->route('order.index');
    }
}
```

7. What is the issue with the following code and how would you fix it?

```php
class UserController extends Controller {
    public function show($id) {
        $user = User::find(id);
        return view('user.profile', compact('user'));
    }
}
```

## Make server/storage and server/bootstrap folder writeable.

If you're recieving the following error when attempting to run Laravel:

> The stream or file "/var/www/html/storage/logs/laravel.log" could not be opened: failed to open stream: Permission denied

Run `sudo chmod o+w ./server/storage/ -R && mkdir -p ./server/boostrap/cache && sudo chmod o+w ./server/bootstrap/ -R` to correct this issue.

Submission and Clean up
=======================

You've completed the assignment—well done! Now, follow these final steps to package your work and clean up your environment.

## Packaging Your Work

To ensure all necessary files, including the `.git` directory, are included, **please refer to the packaging instructions in the `README.md` file at the root of the project**. The provided scripts will handle the process automatically.

## Clean Up

To remove installed dependencies, run:

```sh
$ rm -rf client/node_modules && rm -rf server/vendor`
```

To stop and remove all Docker containers and volumes used during the assignment, run:

```sh
$ docker compose down -v
```

To remove all Docker images downloaded or built during the assignment (optional):

```
$ docker rmi $(docker images -q)
```

*Note: If you have other Docker projects, you may want to skip this step.*

After packaging your assignment using the provided instructions in `README.md` at the root of the project, submit the `interview_assignment.zip` file as per the instructions.

Thank you for completing the assessment. 🚀
