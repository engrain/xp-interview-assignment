<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class MyNewResource extends Controller
{
    /**
     * List people
     *
     * @param Request $request
     * @return mixed
     */
    public function welcome(Request $request)
    {
        return view('my-new-resource/welcome');
    }
}
